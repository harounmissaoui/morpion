# Règle du jeu
Les joueurs écrivent à tour de rôle leur symbole sur une grille sur l'écran de la carte STM32 sur laquelle nous jouons déjà, elle est en fait divisée en 9 cases. Le premier qui parvient à aligner trois de ses symboles horizontalement, verticalement ou en diagonale, il affichera automatiquement le résultat si le joueur 1 est ganges ou le joueur 2 gagne sinon ils recommencent en appuyant sur le bouton noir.

# Fonctionnement des programmes
Pour fonctionner sur deux cartes, nous avons fait le choix de développer deux versions distinctes d'un même programme : la version Maitre et la version Esclave. Il conviendra de compiler chacune des deux versions sur un des cartes utilisées pour jouer au Morpion.


## Programme maître
La carte qui se voit attribuer ce programme a pour responsabilité de gérer le déroulement du jeu, de transmettre les informations du jeu à la carte Esclave et de recevoir des informations de cette dernière.


## Programme Esclave
La carte qui se voit attribuer ce programme doit recevoir les données de jeu émises par la première carte (programme Maitre).