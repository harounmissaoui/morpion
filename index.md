# Présentation du mini-projet d'informatique industrielle : MORPION

## Présentation - Objectif
### 1) Une seule carte et sans liaison série
Ce projet a été réalisé par Haroun Missaoui, élève du M1 GII de l'UFTAM, dans le cadre de l'UE d'Informatique Industrielle.
L'objectif de ce mini projet est de recréer une version simplifiée d'un jeu auquel vous avez probablement joué durant votre enfance : "Le jeux de morpion". 
Le morpion est un jeu de réflexion se pratiquant à deux joueurs au tour par tour et dont le but est de créer le premier un alignement sur une grille. 
Le jeu se joue généralement avec papier et crayon mais grâce à notre matière informatique industriel et notre carte STM32 nous pouvons réaliser ce jeu. 
Voici une image qui vous aidera certainement à mieux comprendre de quoi je parle : 

![screenshot](deux.jpg)  

### 2) Deux  cartes avec liaison série
Ensuite nous avons optimisé notre projet avec une liaison série, c'est la communication entre 2 microcontrôleurs (UART6), qui doivent échanger des informations pour permettre à chaque joueur de jouer sur sa propre carte en utilisant une fonction Maître et fonction Esclave.Voici une image qui vous aidera certainement à mieux comprendre de quoi je parle :
 
>>>>>   ![screenshot](l1.jpg) 
 

>![screenshot](deuxx.png)

### 3) Les taches
* Affichage

* Joueur1


* Joueur2


* Game Over 


*  HAL_UART_RxCpltCallback


### 4) Schéma d'interaction des tâches
![screenshot](Cap.jpg)


Les fichiers de code du mini-projet sont disponibles à l'adresse suivante : 
<https://gitlab.com/harounmissaoui/morpion>




    