/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stm32746g_discovery_lcd.h"
#include "stm32746g_discovery_ts.h"
#include "stdio.h"
#define USER_BP_GPIO_Port GPIOI
#define BP_H_Pin GPIO_PIN_11
#define BP_H_GPIO_Port GPIOI
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

DMA2D_HandleTypeDef hdma2d;

I2C_HandleTypeDef hi2c3;

LTDC_HandleTypeDef hltdc;

RTC_HandleTypeDef hrtc;

TIM_HandleTypeDef htim1;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart6;

SDRAM_HandleTypeDef hsdram1;

osThreadId AffichageTaskHandle;
osThreadId Joueur1TaskHandle;
osThreadId Joueur2Handle;
osThreadId GameOverHandle;
osMessageQId g1Handle;
osMessageQId g2Handle;
osMessageQId g3Handle;
osMutexId myMutexLCDHandle;
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C3_Init(void);
static void MX_LTDC_Init(void);
static void MX_RTC_Init(void);
static void MX_TIM1_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_FMC_Init(void);
static void MX_DMA2D_Init(void);
static void MX_USART6_UART_Init(void);
void fonctionaffichageTask(void const *argument);
void fonctionjoueur1(void const *argument);
void fonctionjoueur2(void const *argument);
void fonctiongameover(void const *argument);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
static char j1[9];
static char j2[9];
uint8_t cpt[9] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
uint8_t rxbuffer[100];
uint8_t txbuffer;
/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void) {
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_I2C3_Init();
	MX_LTDC_Init();
	MX_RTC_Init();
	MX_TIM1_Init();
	MX_USART1_UART_Init();
	MX_FMC_Init();
	MX_DMA2D_Init();
	MX_USART6_UART_Init();
	/* USER CODE BEGIN 2 */
	HAL_UART_Receive_IT(&huart6, &txbuffer, 1);
	//Initialisation de l'écran LCD, avec un fond vert, une police de 24 et une écriture rouge
	BSP_LCD_Init();
	BSP_LCD_LayerDefaultInit(0, LCD_FB_START_ADDRESS);
	BSP_LCD_LayerDefaultInit(1,
	LCD_FB_START_ADDRESS + BSP_LCD_GetXSize() * BSP_LCD_GetYSize() * 4);
	BSP_LCD_DisplayOn();
	BSP_LCD_SelectLayer(1);
	BSP_LCD_Clear(LCD_COLOR_DARKGREEN);
	BSP_LCD_SetFont(&Font24);
	BSP_LCD_SetTextColor(LCD_COLOR_DARKRED);
	BSP_LCD_SetBackColor(LCD_COLOR_DARKGREEN);

	//Initialisation de la dalle tactile
	BSP_TS_Init(BSP_LCD_GetXSize(), BSP_LCD_GetYSize());

	/* USER CODE END 2 */

	/* Create the mutex(es) */
	/* definition and creation of myMutexLCD */
	osMutexDef(myMutexLCD);
	myMutexLCDHandle = osMutexCreate(osMutex(myMutexLCD));

	/* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */

	/* USER CODE END RTOS_MUTEX */

	/* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
	/* USER CODE END RTOS_SEMAPHORES */

	/* USER CODE BEGIN RTOS_TIMERS */
	/* start timers, add new ones, ... */
	/* USER CODE END RTOS_TIMERS */

	/* Create the queue(s) */
	/* definition and creation of g1 */
	osMessageQDef(g1, 8, uint8_t);
	g1Handle = osMessageCreate(osMessageQ(g1), NULL);

	/* definition and creation of g2 */
	osMessageQDef(g2, 8, uint8_t);
	g2Handle = osMessageCreate(osMessageQ(g2), NULL);

	/* definition and creation of g3 */
	osMessageQDef(g3, 8, uint8_t);
	g3Handle = osMessageCreate(osMessageQ(g3), NULL);

	/* USER CODE BEGIN RTOS_QUEUES */
	/* add queues, ... */
	/* USER CODE END RTOS_QUEUES */

	/* Create the thread(s) */
	/* definition and creation of AffichageTask */
	osThreadDef(AffichageTask, fonctionaffichageTask, osPriorityHigh, 0, 1024);
	AffichageTaskHandle = osThreadCreate(osThread(AffichageTask), NULL);

	/* definition and creation of Joueur1Task */
	osThreadDef(Joueur1Task, fonctionjoueur1, osPriorityNormal, 0, 1024);
	Joueur1TaskHandle = osThreadCreate(osThread(Joueur1Task), NULL);

	/* definition and creation of Joueur2 */
	osThreadDef(Joueur2, fonctionjoueur2, osPriorityLow, 0, 1024);
	Joueur2Handle = osThreadCreate(osThread(Joueur2), NULL);

	/* definition and creation of GameOver */
	osThreadDef(GameOver, fonctiongameover, osPriorityHigh, 0, 1024);
	GameOverHandle = osThreadCreate(osThread(GameOver), NULL);

	/* USER CODE BEGIN RTOS_THREADS */
	/* add threads, ... */
	/* USER CODE END RTOS_THREADS */

	/* Start scheduler */
	osKernelStart();

	/* We should never get here as control is now taken by the scheduler */
	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1) {

		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };
	RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = { 0 };

	/** Configure LSE Drive Capability
	 */
	HAL_PWR_EnableBkUpAccess();
	/** Configure the main internal regulator output voltage
	 */
	__HAL_RCC_PWR_CLK_ENABLE();
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
	/** Initializes the RCC Oscillators according to the specified parameters
	 * in the RCC_OscInitTypeDef structure.
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI
			| RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.LSIState = RCC_LSI_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 25;
	RCC_OscInitStruct.PLL.PLLN = 400;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 9;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler();
	}
	/** Activate the Over-Drive mode
	 */
	if (HAL_PWREx_EnableOverDrive() != HAL_OK) {
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB buses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_6) != HAL_OK) {
		Error_Handler();
	}
	PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_LTDC
			| RCC_PERIPHCLK_RTC | RCC_PERIPHCLK_USART1 | RCC_PERIPHCLK_USART6
			| RCC_PERIPHCLK_I2C3;
	PeriphClkInitStruct.PLLSAI.PLLSAIN = 384;
	PeriphClkInitStruct.PLLSAI.PLLSAIR = 5;
	PeriphClkInitStruct.PLLSAI.PLLSAIQ = 2;
	PeriphClkInitStruct.PLLSAI.PLLSAIP = RCC_PLLSAIP_DIV8;
	PeriphClkInitStruct.PLLSAIDivQ = 1;
	PeriphClkInitStruct.PLLSAIDivR = RCC_PLLSAIDIVR_8;
	PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
	PeriphClkInitStruct.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
	PeriphClkInitStruct.Usart6ClockSelection = RCC_USART6CLKSOURCE_PCLK2;
	PeriphClkInitStruct.I2c3ClockSelection = RCC_I2C3CLKSOURCE_PCLK1;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK) {
		Error_Handler();
	}
}

/**
 * @brief DMA2D Initialization Function
 * @param None
 * @retval None
 */
static void MX_DMA2D_Init(void) {

	/* USER CODE BEGIN DMA2D_Init 0 */

	/* USER CODE END DMA2D_Init 0 */

	/* USER CODE BEGIN DMA2D_Init 1 */

	/* USER CODE END DMA2D_Init 1 */
	hdma2d.Instance = DMA2D;
	hdma2d.Init.Mode = DMA2D_M2M;
	hdma2d.Init.ColorMode = DMA2D_OUTPUT_ARGB8888;
	hdma2d.Init.OutputOffset = 0;
	hdma2d.LayerCfg[1].InputOffset = 0;
	hdma2d.LayerCfg[1].InputColorMode = DMA2D_INPUT_ARGB8888;
	hdma2d.LayerCfg[1].AlphaMode = DMA2D_NO_MODIF_ALPHA;
	hdma2d.LayerCfg[1].InputAlpha = 0;
	if (HAL_DMA2D_Init(&hdma2d) != HAL_OK) {
		Error_Handler();
	}
	if (HAL_DMA2D_ConfigLayer(&hdma2d, 1) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN DMA2D_Init 2 */

	/* USER CODE END DMA2D_Init 2 */

}

/**
 * @brief I2C3 Initialization Function
 * @param None
 * @retval None
 */
static void MX_I2C3_Init(void) {

	/* USER CODE BEGIN I2C3_Init 0 */

	/* USER CODE END I2C3_Init 0 */

	/* USER CODE BEGIN I2C3_Init 1 */

	/* USER CODE END I2C3_Init 1 */
	hi2c3.Instance = I2C3;
	hi2c3.Init.Timing = 0x00C0EAFF;
	hi2c3.Init.OwnAddress1 = 0;
	hi2c3.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	hi2c3.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	hi2c3.Init.OwnAddress2 = 0;
	hi2c3.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
	hi2c3.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	hi2c3.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
	if (HAL_I2C_Init(&hi2c3) != HAL_OK) {
		Error_Handler();
	}
	/** Configure Analogue filter
	 */
	if (HAL_I2CEx_ConfigAnalogFilter(&hi2c3, I2C_ANALOGFILTER_ENABLE)
			!= HAL_OK) {
		Error_Handler();
	}
	/** Configure Digital filter
	 */
	if (HAL_I2CEx_ConfigDigitalFilter(&hi2c3, 0) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN I2C3_Init 2 */

	/* USER CODE END I2C3_Init 2 */

}

/**
 * @brief LTDC Initialization Function
 * @param None
 * @retval None
 */
static void MX_LTDC_Init(void) {

	/* USER CODE BEGIN LTDC_Init 0 */

	/* USER CODE END LTDC_Init 0 */

	LTDC_LayerCfgTypeDef pLayerCfg = { 0 };

	/* USER CODE BEGIN LTDC_Init 1 */

	/* USER CODE END LTDC_Init 1 */
	hltdc.Instance = LTDC;
	hltdc.Init.HSPolarity = LTDC_HSPOLARITY_AL;
	hltdc.Init.VSPolarity = LTDC_VSPOLARITY_AL;
	hltdc.Init.DEPolarity = LTDC_DEPOLARITY_AL;
	hltdc.Init.PCPolarity = LTDC_PCPOLARITY_IPC;
	hltdc.Init.HorizontalSync = 40;
	hltdc.Init.VerticalSync = 9;
	hltdc.Init.AccumulatedHBP = 53;
	hltdc.Init.AccumulatedVBP = 11;
	hltdc.Init.AccumulatedActiveW = 533;
	hltdc.Init.AccumulatedActiveH = 283;
	hltdc.Init.TotalWidth = 565;
	hltdc.Init.TotalHeigh = 285;
	hltdc.Init.Backcolor.Blue = 0;
	hltdc.Init.Backcolor.Green = 0;
	hltdc.Init.Backcolor.Red = 0;
	if (HAL_LTDC_Init(&hltdc) != HAL_OK) {
		Error_Handler();
	}
	pLayerCfg.WindowX0 = 0;
	pLayerCfg.WindowX1 = 480;
	pLayerCfg.WindowY0 = 0;
	pLayerCfg.WindowY1 = 272;
	pLayerCfg.PixelFormat = LTDC_PIXEL_FORMAT_RGB565;
	pLayerCfg.Alpha = 255;
	pLayerCfg.Alpha0 = 0;
	pLayerCfg.BlendingFactor1 = LTDC_BLENDING_FACTOR1_PAxCA;
	pLayerCfg.BlendingFactor2 = LTDC_BLENDING_FACTOR2_PAxCA;
	pLayerCfg.FBStartAdress = 0xC0000000;
	pLayerCfg.ImageWidth = 480;
	pLayerCfg.ImageHeight = 272;
	pLayerCfg.Backcolor.Blue = 0;
	pLayerCfg.Backcolor.Green = 0;
	pLayerCfg.Backcolor.Red = 0;
	if (HAL_LTDC_ConfigLayer(&hltdc, &pLayerCfg, 0) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN LTDC_Init 2 */

	/* USER CODE END LTDC_Init 2 */

}

/**
 * @brief RTC Initialization Function
 * @param None
 * @retval None
 */
static void MX_RTC_Init(void) {

	/* USER CODE BEGIN RTC_Init 0 */

	/* USER CODE END RTC_Init 0 */

	RTC_TimeTypeDef sTime = { 0 };
	RTC_DateTypeDef sDate = { 0 };
	RTC_AlarmTypeDef sAlarm = { 0 };

	/* USER CODE BEGIN RTC_Init 1 */

	/* USER CODE END RTC_Init 1 */
	/** Initialize RTC Only
	 */
	hrtc.Instance = RTC;
	hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
	hrtc.Init.AsynchPrediv = 127;
	hrtc.Init.SynchPrediv = 255;
	hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
	hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
	hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
	if (HAL_RTC_Init(&hrtc) != HAL_OK) {
		Error_Handler();
	}

	/* USER CODE BEGIN Check_RTC_BKUP */

	/* USER CODE END Check_RTC_BKUP */

	/** Initialize RTC and set the Time and Date
	 */
	sTime.Hours = 0x0;
	sTime.Minutes = 0x0;
	sTime.Seconds = 0x0;
	sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	sTime.StoreOperation = RTC_STOREOPERATION_RESET;
	if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK) {
		Error_Handler();
	}
	sDate.WeekDay = RTC_WEEKDAY_MONDAY;
	sDate.Month = RTC_MONTH_JANUARY;
	sDate.Date = 0x1;
	sDate.Year = 0x0;
	if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK) {
		Error_Handler();
	}
	/** Enable the Alarm A
	 */
	sAlarm.AlarmTime.Hours = 0x0;
	sAlarm.AlarmTime.Minutes = 0x0;
	sAlarm.AlarmTime.Seconds = 0x0;
	sAlarm.AlarmTime.SubSeconds = 0x0;
	sAlarm.AlarmTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	sAlarm.AlarmTime.StoreOperation = RTC_STOREOPERATION_RESET;
	sAlarm.AlarmMask = RTC_ALARMMASK_NONE;
	sAlarm.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
	sAlarm.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE;
	sAlarm.AlarmDateWeekDay = 0x1;
	sAlarm.Alarm = RTC_ALARM_A;
	if (HAL_RTC_SetAlarm(&hrtc, &sAlarm, RTC_FORMAT_BCD) != HAL_OK) {
		Error_Handler();
	}
	/** Enable the Alarm B
	 */
	sAlarm.Alarm = RTC_ALARM_B;
	if (HAL_RTC_SetAlarm(&hrtc, &sAlarm, RTC_FORMAT_BCD) != HAL_OK) {
		Error_Handler();
	}
	/** Enable the TimeStamp
	 */
	if (HAL_RTCEx_SetTimeStamp(&hrtc, RTC_TIMESTAMPEDGE_RISING,
	RTC_TIMESTAMPPIN_POS1) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN RTC_Init 2 */

	/* USER CODE END RTC_Init 2 */

}

/**
 * @brief TIM1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM1_Init(void) {

	/* USER CODE BEGIN TIM1_Init 0 */

	/* USER CODE END TIM1_Init 0 */

	TIM_ClockConfigTypeDef sClockSourceConfig = { 0 };
	TIM_MasterConfigTypeDef sMasterConfig = { 0 };

	/* USER CODE BEGIN TIM1_Init 1 */

	/* USER CODE END TIM1_Init 1 */
	htim1.Instance = TIM1;
	htim1.Init.Prescaler = 0;
	htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim1.Init.Period = 65535;
	htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim1.Init.RepetitionCounter = 0;
	htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&htim1) != HAL_OK) {
		Error_Handler();
	}
	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK) {
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig)
			!= HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN TIM1_Init 2 */

	/* USER CODE END TIM1_Init 2 */

}

/**
 * @brief USART1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_USART1_UART_Init(void) {

	/* USER CODE BEGIN USART1_Init 0 */

	/* USER CODE END USART1_Init 0 */

	/* USER CODE BEGIN USART1_Init 1 */

	/* USER CODE END USART1_Init 1 */
	huart1.Instance = USART1;
	huart1.Init.BaudRate = 115200;
	huart1.Init.WordLength = UART_WORDLENGTH_8B;
	huart1.Init.StopBits = UART_STOPBITS_1;
	huart1.Init.Parity = UART_PARITY_NONE;
	huart1.Init.Mode = UART_MODE_TX_RX;
	huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart1.Init.OverSampling = UART_OVERSAMPLING_16;
	huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init(&huart1) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN USART1_Init 2 */

	/* USER CODE END USART1_Init 2 */

}

/**
 * @brief USART6 Initialization Function
 * @param None
 * @retval None
 */
static void MX_USART6_UART_Init(void) {

	/* USER CODE BEGIN USART6_Init 0 */

	/* USER CODE END USART6_Init 0 */

	/* USER CODE BEGIN USART6_Init 1 */

	/* USER CODE END USART6_Init 1 */
	huart6.Instance = USART6;
	huart6.Init.BaudRate = 115200;
	huart6.Init.WordLength = UART_WORDLENGTH_8B;
	huart6.Init.StopBits = UART_STOPBITS_1;
	huart6.Init.Parity = UART_PARITY_NONE;
	huart6.Init.Mode = UART_MODE_TX_RX;
	huart6.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart6.Init.OverSampling = UART_OVERSAMPLING_16;
	huart6.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart6.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init(&huart6) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN USART6_Init 2 */

	/* USER CODE END USART6_Init 2 */

}

/* FMC initialization function */
static void MX_FMC_Init(void) {

	/* USER CODE BEGIN FMC_Init 0 */

	/* USER CODE END FMC_Init 0 */

	FMC_SDRAM_TimingTypeDef SdramTiming = { 0 };

	/* USER CODE BEGIN FMC_Init 1 */

	/* USER CODE END FMC_Init 1 */

	/** Perform the SDRAM1 memory initialization sequence
	 */
	hsdram1.Instance = FMC_SDRAM_DEVICE;
	/* hsdram1.Init */
	hsdram1.Init.SDBank = FMC_SDRAM_BANK1;
	hsdram1.Init.ColumnBitsNumber = FMC_SDRAM_COLUMN_BITS_NUM_8;
	hsdram1.Init.RowBitsNumber = FMC_SDRAM_ROW_BITS_NUM_12;
	hsdram1.Init.MemoryDataWidth = FMC_SDRAM_MEM_BUS_WIDTH_16;
	hsdram1.Init.InternalBankNumber = FMC_SDRAM_INTERN_BANKS_NUM_4;
	hsdram1.Init.CASLatency = FMC_SDRAM_CAS_LATENCY_1;
	hsdram1.Init.WriteProtection = FMC_SDRAM_WRITE_PROTECTION_DISABLE;
	hsdram1.Init.SDClockPeriod = FMC_SDRAM_CLOCK_DISABLE;
	hsdram1.Init.ReadBurst = FMC_SDRAM_RBURST_DISABLE;
	hsdram1.Init.ReadPipeDelay = FMC_SDRAM_RPIPE_DELAY_0;
	/* SdramTiming */
	SdramTiming.LoadToActiveDelay = 16;
	SdramTiming.ExitSelfRefreshDelay = 16;
	SdramTiming.SelfRefreshTime = 16;
	SdramTiming.RowCycleDelay = 16;
	SdramTiming.WriteRecoveryTime = 16;
	SdramTiming.RPDelay = 16;
	SdramTiming.RCDDelay = 16;

	if (HAL_SDRAM_Init(&hsdram1, &SdramTiming) != HAL_OK) {
		Error_Handler();
	}

	/* USER CODE BEGIN FMC_Init 2 */

	/* USER CODE END FMC_Init 2 */
}

/**
 * @brief GPIO Initialization Function
 * @param None
 * @retval None
 */
static void MX_GPIO_Init(void) {
	GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOE_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOG_CLK_ENABLE();
	__HAL_RCC_GPIOJ_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOI_CLK_ENABLE();
	__HAL_RCC_GPIOK_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOF_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(OTG_FS_PowerSwitchOn_GPIO_Port, OTG_FS_PowerSwitchOn_Pin,
			GPIO_PIN_SET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(LCD_BL_CTRL_GPIO_Port, LCD_BL_CTRL_Pin, GPIO_PIN_SET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(LCD_DISP_GPIO_Port, LCD_DISP_Pin, GPIO_PIN_SET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(EXT_RST_GPIO_Port, EXT_RST_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin : PE3 */
	GPIO_InitStruct.Pin = GPIO_PIN_3;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

	/*Configure GPIO pins : ARDUINO_SCL_D15_Pin ARDUINO_SDA_D14_Pin */
	GPIO_InitStruct.Pin = ARDUINO_SCL_D15_Pin | ARDUINO_SDA_D14_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pins : ULPI_D7_Pin ULPI_D6_Pin ULPI_D5_Pin ULPI_D2_Pin
	 ULPI_D1_Pin ULPI_D4_Pin */
	GPIO_InitStruct.Pin = ULPI_D7_Pin | ULPI_D6_Pin | ULPI_D5_Pin | ULPI_D2_Pin
			| ULPI_D1_Pin | ULPI_D4_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF10_OTG_HS;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pin : PB4 */
	GPIO_InitStruct.Pin = GPIO_PIN_4;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = GPIO_AF2_TIM3;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pin : OTG_FS_VBUS_Pin */
	GPIO_InitStruct.Pin = OTG_FS_VBUS_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(OTG_FS_VBUS_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : Audio_INT_Pin */
	GPIO_InitStruct.Pin = Audio_INT_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(Audio_INT_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : OTG_FS_PowerSwitchOn_Pin */
	GPIO_InitStruct.Pin = OTG_FS_PowerSwitchOn_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(OTG_FS_PowerSwitchOn_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : PI2 */
	GPIO_InitStruct.Pin = GPIO_PIN_2;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = GPIO_AF3_TIM8;
	HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

	/*Configure GPIO pin : uSD_Detect_Pin */
	GPIO_InitStruct.Pin = uSD_Detect_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(uSD_Detect_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : LCD_BL_CTRL_Pin */
	GPIO_InitStruct.Pin = LCD_BL_CTRL_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(LCD_BL_CTRL_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : OTG_FS_OverCurrent_Pin */
	GPIO_InitStruct.Pin = OTG_FS_OverCurrent_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(OTG_FS_OverCurrent_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : TP3_Pin NC2_Pin */
	GPIO_InitStruct.Pin = TP3_Pin | NC2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

	/*Configure GPIO pins : LED1_Pin LCD_DISP_Pin */
	GPIO_InitStruct.Pin = LED1_Pin | LCD_DISP_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

	/*Configure GPIO pin : PI0 */
	GPIO_InitStruct.Pin = GPIO_PIN_0;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
	HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

	/*Configure GPIO pin : BP_H_Pin */
	GPIO_InitStruct.Pin = BP_H_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(BP_H_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : LCD_INT_Pin */
	GPIO_InitStruct.Pin = LCD_INT_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(LCD_INT_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : ULPI_NXT_Pin */
	GPIO_InitStruct.Pin = ULPI_NXT_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF10_OTG_HS;
	HAL_GPIO_Init(ULPI_NXT_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : PF7 */
	GPIO_InitStruct.Pin = GPIO_PIN_7;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF8_UART7;
	HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

	/*Configure GPIO pins : PF10 PF9 PF8 */
	GPIO_InitStruct.Pin = GPIO_PIN_10 | GPIO_PIN_9 | GPIO_PIN_8;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

	/*Configure GPIO pins : ULPI_STP_Pin ULPI_DIR_Pin */
	GPIO_InitStruct.Pin = ULPI_STP_Pin | ULPI_DIR_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF10_OTG_HS;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pin : EXT_RST_Pin */
	GPIO_InitStruct.Pin = EXT_RST_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(EXT_RST_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : RMII_RXER_Pin */
	GPIO_InitStruct.Pin = RMII_RXER_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(RMII_RXER_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : PA0 PA4 */
	GPIO_InitStruct.Pin = GPIO_PIN_0 | GPIO_PIN_4;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pins : ULPI_CLK_Pin ULPI_D0_Pin */
	GPIO_InitStruct.Pin = ULPI_CLK_Pin | ULPI_D0_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF10_OTG_HS;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pins : PB14 PB15 */
	GPIO_InitStruct.Pin = GPIO_PIN_14 | GPIO_PIN_15;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/* EXTI interrupt init*/
	HAL_NVIC_SetPriority(EXTI15_10_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
	//HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);

}
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
	HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
	char text[50];
	uint8_t gagne = 0;
	uint8_t joueur = (txbuffer / 10);
	uint8_t har = (txbuffer % 10);

	if (har == 1) {
		if (joueur == 0) {
			sprintf(text, "X");
			BSP_LCD_DisplayStringAt(75, 38, (uint8_t*) text, LEFT_MODE);
			BSP_LCD_SetTextColor(LCD_COLOR_DARKBLUE);
		} else {
			sprintf(text, "O");
			BSP_LCD_DisplayStringAt(75, 38, (uint8_t*) text, LEFT_MODE);
			BSP_LCD_SetTextColor(LCD_COLOR_ORANGE);
		}
	}
	if (har == 2) {
		if (joueur == 0) {
			sprintf(text, "X");
			BSP_LCD_DisplayStringAt(240, 38, (uint8_t*) text, LEFT_MODE);
			BSP_LCD_SetTextColor(LCD_COLOR_DARKBLUE);
		} else {
			sprintf(text, "O");
			BSP_LCD_DisplayStringAt(240, 38, (uint8_t*) text, LEFT_MODE);
			BSP_LCD_SetTextColor(LCD_COLOR_ORANGE);
		}
	}
	if (har == 3) {
		if (joueur == 0) {
			sprintf(text, "X");
			BSP_LCD_DisplayStringAt(400, 38, (uint8_t*) text, LEFT_MODE);
			BSP_LCD_SetTextColor(LCD_COLOR_DARKBLUE);
		} else {
			sprintf(text, "O");
			BSP_LCD_DisplayStringAt(400, 38, (uint8_t*) text, LEFT_MODE);
			BSP_LCD_SetTextColor(LCD_COLOR_ORANGE);
		}
	}
	if (har == 4) {
		if (joueur == 0) {
			sprintf(text, "X");
			BSP_LCD_DisplayStringAt(75, 115, (uint8_t*) text, LEFT_MODE);
			BSP_LCD_SetTextColor(LCD_COLOR_DARKBLUE);
		} else {
			sprintf(text, "O");
			BSP_LCD_DisplayStringAt(75, 115, (uint8_t*) text, LEFT_MODE);
			BSP_LCD_SetTextColor(LCD_COLOR_ORANGE);
		}
	}
	if (har == 5) {
		if (joueur == 0) {
			sprintf(text, "X");
			BSP_LCD_DisplayStringAt(240, 115, (uint8_t*) text, LEFT_MODE);
			BSP_LCD_SetTextColor(LCD_COLOR_DARKBLUE);
		} else {
			sprintf(text, "O");
			BSP_LCD_DisplayStringAt(240, 115, (uint8_t*) text, LEFT_MODE);
			BSP_LCD_SetTextColor(LCD_COLOR_ORANGE);
		}
	}
	if (har == 6) {
		if (joueur == 0) {
			sprintf(text, "X");
			BSP_LCD_DisplayStringAt(400, 115, (uint8_t*) text, LEFT_MODE);
			BSP_LCD_SetTextColor(LCD_COLOR_DARKBLUE);
		} else {
			sprintf(text, "O");
			BSP_LCD_DisplayStringAt(400, 115, (uint8_t*) text, LEFT_MODE);
			BSP_LCD_SetTextColor(LCD_COLOR_ORANGE);

		}
	}
	if (har == 7) {
		if (joueur == 0) {
			sprintf(text, "X");
			BSP_LCD_DisplayStringAt(75, 210, (uint8_t*) text, LEFT_MODE);
			BSP_LCD_SetTextColor(LCD_COLOR_DARKBLUE);
		} else {
			sprintf(text, "O");
			BSP_LCD_DisplayStringAt(75, 210, (uint8_t*) text, LEFT_MODE);
			BSP_LCD_SetTextColor(LCD_COLOR_ORANGE);
		}
	}
	if (har == 8) {
		if (joueur == 0) {
			sprintf(text, "X");
			BSP_LCD_DisplayStringAt(240, 210, (uint8_t*) text, LEFT_MODE);
			BSP_LCD_SetTextColor(LCD_COLOR_DARKBLUE);
		} else {
			sprintf(text, "O");
			BSP_LCD_DisplayStringAt(240, 210, (uint8_t*) text, LEFT_MODE);
			BSP_LCD_SetTextColor(LCD_COLOR_ORANGE);
		}
	}
	if (har == 9) {
		if (joueur == 0) {
			sprintf(text, "X");
			BSP_LCD_DisplayStringAt(400, 210, (uint8_t*) text, LEFT_MODE);
			BSP_LCD_SetTextColor(LCD_COLOR_DARKBLUE);
		} else {
			sprintf(text, "O");
			BSP_LCD_DisplayStringAt(400, 210, (uint8_t*) text, LEFT_MODE);
			BSP_LCD_SetTextColor(LCD_COLOR_ORANGE);
		}
	}

	//On relance l’interruption
	HAL_UART_Receive_IT(&huart6, &txbuffer, 1);
	HAL_UART_Receive_IT(&huart6, &gagne, 1);

}
/* USER CODE END 4 */

/* USER CODE BEGIN Header_fonctionaffichageTask */
/**
 * @brief  Function implementing the AffichageTask thread.
 * @param  argument: Not used
 * @retval None
 */
/* USER CODE END Header_fonctionaffichageTask */
void fonctionaffichageTask(void const *argument) {
	/* USER CODE BEGIN 5 */
	static TS_StateTypeDef TS_State;
	static uint16_t parite = 1;
	uint8_t cpt1 = 0, cpt2 = 0, cpt3 = 0, cpt4 = 0, cpt5 = 0, cpt6 = 0,
			cpt7 = 0, cpt8 = 0, cpt9 = 0;
	uint8_t Message = 0, ecran_touche, ecran_touche_old = 0;
	uint8_t joueur;

	/* Infinite loop */
	xSemaphoreTake(myMutexLCDHandle, portMAX_DELAY);
	BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
	BSP_LCD_DrawRect(0, 0, 160, 91);
	BSP_LCD_DrawRect(0, 91, 160, 91);
	BSP_LCD_DrawRect(0, 181, 160, 91);
	BSP_LCD_DrawRect(160, 0, 160, 91);
	BSP_LCD_DrawRect(160, 91, 160, 91);
	BSP_LCD_DrawRect(160, 181, 160, 91);
	BSP_LCD_DrawRect(320, 0, 160, 91);
	BSP_LCD_DrawRect(320, 91, 160, 91);
	BSP_LCD_DrawRect(320, 181, 160, 91);
	xSemaphoreGive(myMutexLCDHandle);
	for (;;) {

		txbuffer = 0;
		BSP_TS_GetState(&TS_State);

		ecran_touche = TS_State.touchDetected;
		if ((ecran_touche == 1) && (ecran_touche_old == 0)) {
			// ---- 1ere ligne
			//rectangle 1
			if ((TS_State.touchX[0] >= 0) && (TS_State.touchX[0] <= 160)
					&& (TS_State.touchY[0] <= 91)
					&& (TS_State.touchY[0] >= 0)) {
				cpt1++;
				if (cpt1 == 1) {
					Message = 1;
					parite = parite + 1;
					if (parite % 2 == 0)
						joueur = 0;
					if (parite % 2 != 0)
						joueur = 1;
					txbuffer = Message + joueur * 10;
					HAL_UART_Transmit(&huart6, &txbuffer, 1, 1000);
				}
			}
			//rectangle 2
			if (TS_State.touchX[0] >= 160 && TS_State.touchX[0] <= 320
					&& TS_State.touchY[0] <= 91 && TS_State.touchY[0] >= 0) {
				cpt2++;
				if (cpt2 == 1) {
					Message = 2;
					parite = parite + 1;
					if (parite % 2 == 0)
						joueur = 0;
					if (parite % 2 != 0)
						joueur = 1;
					txbuffer = Message + joueur * 10;
					HAL_UART_Transmit(&huart6, &txbuffer, 1, 1000);
				}
			}
			//rectangle 3
			if (TS_State.touchX[0] >= 320 && TS_State.touchX[0] <= 480
					&& TS_State.touchY[0] <= 91 && TS_State.touchY[0] >= 0) {
				cpt3++;
				if (cpt3 == 1) {
					Message = 3;
					parite = parite + 1;
					if (parite % 2 == 0)
						joueur = 0;
					if (parite % 2 != 0)
						joueur = 1;
					txbuffer = Message + joueur * 10;
					HAL_UART_Transmit(&huart6, &txbuffer, 1, 1000);
				}
			}

			// ---- 2eme ligne
			//rectangle 4
			if (TS_State.touchX[0] >= 0 && TS_State.touchX[0] <= 160
					&& TS_State.touchY[0] >= 91 && TS_State.touchY[0] <= 181) {
				cpt4++;
				if (cpt4 == 1) {
					Message = 4;
					parite = parite + 1;
					if (parite % 2 == 0)
						joueur = 0;
					if (parite % 2 != 0)
						joueur = 1;
					txbuffer = Message + joueur * 10;
					HAL_UART_Transmit(&huart6, &txbuffer, 1, 1000);
				}
			}
			//rectangle 5
			if (TS_State.touchX[0] >= 160 && TS_State.touchX[0] <= 320
					&& TS_State.touchY[0] >= 91 && TS_State.touchY[0] <= 181) {
				cpt5++;
				if (cpt5 == 1) {
					Message = 5;
					parite = parite + 1;
					if (parite % 2 == 0)
						joueur = 0;
					if (parite % 2 != 0)
						joueur = 1;
					txbuffer = Message + joueur * 10;
					HAL_UART_Transmit(&huart6, &txbuffer, 1, 1000);
				}
			}
			//rectangle 6
			if (TS_State.touchX[0] >= 320 && TS_State.touchX[0] <= 480
					&& TS_State.touchY[0] >= 91 && TS_State.touchY[0] <= 181) {
				cpt6++;
				if (cpt6 == 1) {
					Message = 6;
					parite = parite + 1;
					if (parite % 2 == 0)
						joueur = 0;
					if (parite % 2 != 0)
						joueur = 1;
					txbuffer = Message + joueur * 10;
					HAL_UART_Transmit(&huart6, &txbuffer, 1, 1000);
				}
			}

			// ---- 3eme ligne
			//rectangle 7
			if (TS_State.touchX[0] >= 0 && TS_State.touchX[0] <= 160
					&& TS_State.touchY[0] >= 181 && TS_State.touchY[0] <= 272) {
				cpt7++;
				if (cpt7 == 1) {
					Message = 7;
					parite = parite + 1;
					if (parite % 2 == 0)
						joueur = 0;
					if (parite % 2 != 0)
						joueur = 1;
					txbuffer = Message + joueur * 10;
					HAL_UART_Transmit(&huart6, &txbuffer, 1, 1000);
				}
			}
			//rectangle 8
			if (TS_State.touchX[0] >= 160 && TS_State.touchX[0] <= 320
					&& TS_State.touchY[0] >= 181 && TS_State.touchY[0] <= 272) {
				cpt8++;
				if (cpt8 == 1) {
					Message = 8;
					parite = parite + 1;
					if (parite % 2 == 0)
						joueur = 0;
					if (parite % 2 != 0)
						joueur = 1;
					txbuffer = Message + joueur * 10;
					HAL_UART_Transmit(&huart6, &txbuffer, 1, 1000);
				}
			}
			//rectangle 9
			if (TS_State.touchX[0] >= 320 && TS_State.touchX[0] <= 480
					&& TS_State.touchY[0] >= 181 && TS_State.touchY[0] <= 272) {
				cpt9++;
				if (cpt9 == 1) {
					Message = 9;
					parite = parite + 1;
					if (parite % 2 == 0)
						joueur = 0;
					if (parite % 2 != 0)
						joueur = 1;
					txbuffer = Message + joueur * 10;
					HAL_UART_Transmit(&huart6, &txbuffer, 1, 1000);
				}
			}

			if (joueur == 0) {
				xQueueSend(g1Handle, &Message, portMAX_DELAY);
			}
			if (joueur == 1) {
				xQueueSend(g2Handle, &Message, portMAX_DELAY);
			}
		}
		ecran_touche_old = ecran_touche;
		osDelay(50);
	}

	/* USER CODE END 5 */
}

/* USER CODE BEGIN Header_fonctionjoueur1 */
/**
 * @brief Function implementing the Joueur1Task thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_fonctionjoueur1 */
void fonctionjoueur1(void const *argument) {
	/* USER CODE BEGIN fonctionjoueur1 */
	char text[10];
	uint8_t Message;
	/* Infinite loop */
	for (;;) {

		xQueueReceive(g1Handle, &Message, portMAX_DELAY);
		if (j1[Message] == 0 && j2[Message] == 0) {
			j1[Message] = 'X';
			sprintf(text, "%c", j1[Message]);
			BSP_LCD_SetTextColor(LCD_COLOR_DARKBLUE);
			cpt[Message] = 1;

			if (Message == 1)
				BSP_LCD_DisplayStringAt(75, 38, (uint8_t*) text, LEFT_MODE);

			if (Message == 2)
				BSP_LCD_DisplayStringAt(240, 38, (uint8_t*) text, LEFT_MODE);

			if (Message == 3)
				BSP_LCD_DisplayStringAt(400, 38, (uint8_t*) text, LEFT_MODE);

			if (Message == 4)
				BSP_LCD_DisplayStringAt(75, 115, (uint8_t*) text, LEFT_MODE);

			if (Message == 5)
				BSP_LCD_DisplayStringAt(240, 115, (uint8_t*) text, LEFT_MODE);

			if (Message == 6)
				BSP_LCD_DisplayStringAt(400, 115, (uint8_t*) text, LEFT_MODE);

			if (Message == 7)
				BSP_LCD_DisplayStringAt(75, 210, (uint8_t*) text, LEFT_MODE);

			if (Message == 8)
				BSP_LCD_DisplayStringAt(240, 210, (uint8_t*) text, LEFT_MODE);

			if (Message == 9)
				BSP_LCD_DisplayStringAt(400, 210, (uint8_t*) text, LEFT_MODE);
		}
	}

	/* USER CODE END fonctionjoueur1 */
}

/* USER CODE BEGIN Header_fonctionjoueur2 */
/**
 * @brief Function implementing the Joueur2 thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_fonctionjoueur2 */
void fonctionjoueur2(void const *argument) {
	/* USER CODE BEGIN fonctionjoueur2 */
	char text[10];
	uint8_t Message;
	/* Infinite loop */
	for (;;) {
		xQueueReceive(g2Handle, &Message, portMAX_DELAY);
		if (j2[Message] == 0 && j1[Message] == 0) {
			j2[Message] = 'O';
			sprintf(text, "%c", j2[Message]);
			cpt[Message] = 1;
			BSP_LCD_SetTextColor(LCD_COLOR_ORANGE);

			if (Message == 1)
				BSP_LCD_DisplayStringAt(75, 38, (uint8_t*) text, LEFT_MODE);

			if (Message == 2)
				BSP_LCD_DisplayStringAt(240, 38, (uint8_t*) text, LEFT_MODE);

			if (Message == 3)
				BSP_LCD_DisplayStringAt(400, 38, (uint8_t*) text, LEFT_MODE);

			if (Message == 4)
				BSP_LCD_DisplayStringAt(75, 115, (uint8_t*) text, LEFT_MODE);

			if (Message == 5)
				BSP_LCD_DisplayStringAt(240, 115, (uint8_t*) text, LEFT_MODE);

			if (Message == 6)
				BSP_LCD_DisplayStringAt(400, 115, (uint8_t*) text, LEFT_MODE);

			if (Message == 7)
				BSP_LCD_DisplayStringAt(75, 210, (uint8_t*) text, LEFT_MODE);

			if (Message == 8)
				BSP_LCD_DisplayStringAt(240, 210, (uint8_t*) text, LEFT_MODE);

			if (Message == 9)
				BSP_LCD_DisplayStringAt(400, 210, (uint8_t*) text, LEFT_MODE);
		}
	}

	/* USER CODE END fonctionjoueur2 */
}

/* USER CODE BEGIN Header_fonctiongameover */
/**
 * @brief Function implementing the GameOver thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_fonctiongameover */
void fonctiongameover(void const *argument) {
	/* USER CODE BEGIN fonctiongameover */
	char text[50];
	char text1[50];
	uint8_t gagne = 0;
	/* Infinite loop */
	for (;;) {
		if (((j1[1] == j1[2]) && (j1[2] == j1[3]) && (j1[1] != 0))
				|| ((j1[4] == j1[5]) && (j1[5] == j1[6]) && (j1[4] != 0))
				|| ((j1[7] == j1[8]) && (j1[8] == j1[9]) && (j1[7] != 0))
				|| ((j1[1] == j1[5]) && (j1[5] == j1[9]) && (j1[1] != 0))
				|| ((j1[3] == j1[5]) && (j1[5] == j1[7]) && (j1[3] != 0))
				|| ((j1[1] == j1[4]) && (j1[4] == j1[7]) && (j1[1] != 0))
				|| ((j1[3] == j1[6]) && (j1[6] == j1[9]) && (j1[3] != 0))
				|| ((j1[2] == j1[5]) && (j1[5] == j1[8]) && (j1[2] != 0))) {
			gagne = 1;

			sprintf(text, "GAME OVER, Joueur 1 gagne");
			BSP_LCD_Clear(LCD_COLOR_DARKGREEN);
			BSP_LCD_SetTextColor(LCD_COLOR_RED);
			BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
			BSP_LCD_DisplayStringAt(30, 100, (uint8_t*) text, LEFT_MODE);
			HAL_UART_Transmit(&huart6, &gagne, 1, 1000);
			vTaskDelete(Joueur1TaskHandle);
			vTaskDelete(Joueur2Handle);
			vTaskDelete(AffichageTaskHandle);
		}

		if (((j2[1] == j2[2]) && (j2[2] == j2[3]) && (j2[1] != 0))
				|| ((j2[4] == j2[5]) && (j2[5] == j2[6]) && (j2[4] != 0))
				|| ((j2[7] == j2[8]) && (j2[8] == j2[9]) && (j2[7] != 0))
				|| ((j2[1] == j2[5]) && (j2[5] == j2[9]) && (j2[1] != 0))
				|| ((j2[3] == j2[5]) && (j2[5] == j2[7]) && (j2[3] != 0))
				|| ((j2[1] == j2[4]) && (j2[4] == j2[7]) && (j2[1] != 0))
				|| ((j2[3] == j2[6]) && (j2[6] == j2[9]) && (j2[3] != 0))
				|| ((j2[2] == j2[5]) && (j2[5] == j2[8]) && (j2[2] != 0))) {
			gagne = 2;
			HAL_UART_Transmit(&huart6, &gagne, 1, 1000);
			sprintf(text, "GAME OVER, Joueur 2 gagne");
			BSP_LCD_Clear(LCD_COLOR_DARKGREEN);
			BSP_LCD_SetTextColor(LCD_COLOR_RED);
			BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
			BSP_LCD_DisplayStringAt(30, 100, (uint8_t*) text, LEFT_MODE);
			HAL_UART_Transmit(&huart6, &gagne, 1, 1000);
			vTaskDelete(Joueur1TaskHandle);
			vTaskDelete(Joueur2Handle);
			vTaskDelete(AffichageTaskHandle);
		}

		if ((cpt[1] == 1) && (cpt[2] == 1) && (cpt[3] == 1) && (cpt[4] == 1)
				&& (cpt[5] == 1) && (cpt[6] == 1) && (cpt[7] == 1)
				&& (cpt[8] == 1) && (cpt[9] == 1) && (gagne == 0)) {
			HAL_UART_Transmit(&huart6, &gagne, 1, 1000);
			sprintf(text, "Match null, recommencer");
			sprintf(text1, "*Appuyer sur le bouton noir*");
			BSP_LCD_Clear(LCD_COLOR_DARKGREEN);
			BSP_LCD_SetTextColor(LCD_COLOR_RED);
			BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
			BSP_LCD_DisplayStringAt(40, 100, (uint8_t*) text, LEFT_MODE);
			BSP_LCD_DisplayStringAt(0, 200, (uint8_t*) text1, LEFT_MODE);
			vTaskDelete(Joueur1TaskHandle);
			vTaskDelete(Joueur2Handle);
			vTaskDelete(AffichageTaskHandle);
		}
		osDelay(1);
	}
	/* USER CODE END fonctiongameover */
}

/**
 * @brief  Period elapsed callback in non blocking mode
 * @note   This function is called  when TIM6 interrupt took place, inside
 * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
 * a global variable "uwTick" used as application time base.
 * @param  htim : TIM handle
 * @retval None
 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	/* USER CODE BEGIN Callback 0 */

	/* USER CODE END Callback 0 */
	if (htim->Instance == TIM6) {
		HAL_IncTick();
	}
	/* USER CODE BEGIN Callback 1 */

	/* USER CODE END Callback 1 */
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void) {
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1) {
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
